class TowersOfHanoi
  attr_reader :towers

  def initialize
    @towers = [[3, 2, 1], [], []]
  end

  def move(from_tower, to_tower)
    @towers[to_tower] << @towers[from_tower].pop
  end

  def play
    until won?
      render

      puts 'Remove disc from which tower?'
      from_tower = gets.chomp.to_i
      if @towers[from_tower].nil?
        puts "Tower #{from_tower} does not exist\n\n"
        next
      end

      puts 'Place disc onto which tower?'
      to_tower = gets.chomp.to_i
      if @towers[to_tower].nil?
        puts "Tower #{to_tower} does not exist\n\n"
        next
      end

      if valid_move?(from_tower, to_tower)
        move(from_tower, to_tower)
      else
        puts "Invalid move \n\n"
      end
    end
    puts 'You Win!!!'
  end

  def render
    puts @towers.to_s
  end

  def valid_move?(from_tower, to_tower)
    unless @towers[from_tower].empty?
      return true if @towers[to_tower].empty? ||
                     @towers[from_tower].last < @towers[to_tower].last
    end
    false
  end

  def won?
    @towers == [[], [3, 2, 1], []]
  end
end
